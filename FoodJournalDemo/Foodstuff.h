//
//  Foodstuff.h
//  FoodJournalDemo
//
//  Created by James Cash on 20-07-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EatenFood;

NS_ASSUME_NONNULL_BEGIN

@interface Foodstuff : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Foodstuff+CoreDataProperties.h"
