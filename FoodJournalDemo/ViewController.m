//
//  ViewController.m
//  FoodJournalDemo
//
//  Created by James Cash on 20-07-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
#import "AppDelegate.h"
#import "ViewController.h"
#import "Foodstuff.h"

@interface ViewController () <UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *foodNameField;
@property (weak, nonatomic) IBOutlet UITextField *foodCaloriesField;
@property (weak, nonatomic) IBOutlet UITableView *foodsTable;
@property (strong,nonatomic) NSFetchedResultsController *fetchController;
// TEMPORARY
//@property (strong, nonatomic) NSArray<Foodstuff*>* foods;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //    [self populateSearchResults];
    NSManagedObjectContext *ctx = ((AppDelegate*)[UIApplication sharedApplication].delegate).managedObjectContext;

    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
    req.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];

    self.fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:req managedObjectContext:ctx sectionNameKeyPath:nil cacheName:nil];

    self.fetchController.delegate = self;

    [self.fetchController performFetch:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)populateSearchResults
//{
//    NSManagedObjectContext *ctx = ((AppDelegate*)[UIApplication sharedApplication].delegate).managedObjectContext;
//
//    NSFetchRequest *req = [NSFetchRequest fetchRequestWithEntityName:@"Foodstuff"];
//    req.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
//
//    self.foods = [ctx executeFetchRequest:req error:nil];
//    [self.foodsTable reloadData];
//}

- (IBAction)saveFood:(id)sender {
    NSManagedObjectContext *ctx = ((AppDelegate*)[UIApplication sharedApplication].delegate).managedObjectContext;
    Foodstuff *newFood = [NSEntityDescription insertNewObjectForEntityForName:@"Foodstuff" inManagedObjectContext:ctx];
    newFood.name = self.foodNameField.text;
    newFood.calories = [NSDecimalNumber numberWithFloat:[self.foodCaloriesField.text floatValue]];
    [ctx save:nil];
    //    [self populateSearchResults];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.foodsTable beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.foodsTable insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.foodsTable deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.foodsTable;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
//                    atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.foodsTable endUpdates];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return [self.foods count];
    return self.fetchController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodStuffCell"];

    //    Foodstuff *food = self.foods[indexPath.row];
    Foodstuff *food = [self.fetchController objectAtIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", food.name, food.calories];
    
    return cell;
}

@end
