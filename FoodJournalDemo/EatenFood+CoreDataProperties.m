//
//  EatenFood+CoreDataProperties.m
//  FoodJournalDemo
//
//  Created by James Cash on 20-07-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EatenFood+CoreDataProperties.h"

@implementation EatenFood (CoreDataProperties)

@dynamic eatenAt;
@dynamic food;

@end
