//
//  Foodstuff+CoreDataProperties.h
//  FoodJournalDemo
//
//  Created by James Cash on 20-07-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Foodstuff.h"

NS_ASSUME_NONNULL_BEGIN

@interface Foodstuff (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSDecimalNumber *calories;
@property (nullable, nonatomic, retain) NSSet<EatenFood *> *eaten;

@end

@interface Foodstuff (CoreDataGeneratedAccessors)

- (void)addEatenObject:(EatenFood *)value;
- (void)removeEatenObject:(EatenFood *)value;
- (void)addEaten:(NSSet<EatenFood *> *)values;
- (void)removeEaten:(NSSet<EatenFood *> *)values;

@end

NS_ASSUME_NONNULL_END
